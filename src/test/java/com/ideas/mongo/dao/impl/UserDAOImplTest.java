package com.ideas.mongo.dao.impl;

import com.ideas.mongo.TenantContext;
import com.ideas.mongo.base.BaseSpringTest;
import com.ideas.mongo.dao.Bottle;
import com.ideas.mongo.dao.IUserDAO;
import com.ideas.mongo.dao.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.index.IndexField;
import org.springframework.data.mongodb.core.index.IndexInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class UserDAOImplTest extends BaseSpringTest {

	@Autowired
	private IUserDAO userDAO;

	@Test
	public void test() {
		String firstName = "Shreyas";
		String lastName = "Singh";
		String loginName = "Shreyas.Singh@ideas.com";
		
		// Create user - in default cluster - Choice - didnkus01ina6
		
		User user = getUser(loginName, firstName, lastName);
		user.setBottles(getBottles());
		userDAO.save(user);
		
		List<IndexInfo> indexInfos = mongoTemplate.indexOps(User.class).getIndexInfo();
		System.out.println("Cluster: Choice ---->" + TenantContext.getTenant().getMongoURI());
		System.out.println("DB ---->" + mongoTemplate.getDb().getName());
		for(IndexInfo idxInf : indexInfos) {
			System.out.println("Index Name ---> " + idxInf.getName());
			for(IndexField idxf: idxInf.getIndexFields()) {
				System.out.println("On Field ---> " + idxf.getKey());
			}
			
		}
//		userDAO.delete(user);
		

		//	Create user - in Wyndham cluster - localhost
		TenantContext.setTenant("Wyndham");
		user = getUser(loginName, firstName, lastName);
		user.setBottles(getBottles());
		userDAO.save(user);
		indexInfos = mongoTemplate.indexOps(User.class).getIndexInfo();
		System.out.println("Cluster: Wyndham ---->" + TenantContext.getTenant().getMongoURI());
		System.out.println("DB ---->" + mongoTemplate.getDb().getName());
		for(IndexInfo idxInf : indexInfos) {
			System.out.println("Index Name ---> " + idxInf.getName());
			for(IndexField idxf: idxInf.getIndexFields()) {
				System.out.println("On Field ---> " + idxf.getKey());
			}
			
		}
//		userDAO.delete(user);
		
		
		//	Create user - in OnPremNGICluster database
		TenantContext.setTenant("OnPrem-NGI");
		user = getUser(loginName, firstName, lastName);
		user.setBottles(getBottles());
		userDAO.save(user);
		indexInfos = mongoTemplate.indexOps(User.class).getIndexInfo();
		System.out.println("Cluster: OnPrem-NGI ---->" + TenantContext.getTenant().getMongoURI());
		System.out.println("DB ---->" + mongoTemplate.getDb().getName());
		for(IndexInfo idxInf : indexInfos) {
			System.out.println("Index Name ---> " + idxInf.getName());
			for(IndexField idxf: idxInf.getIndexFields()) {
				System.out.println("On Field ---> " + idxf.getKey());
			}
			
		}
//		userDAO.delete(user);
	}
	
	public static List<Bottle> getBottles() {
		List<Bottle> bottles = new ArrayList<>();
		String pattern = "CamelBack";
		
		Bottle bottle2 = new Bottle();
		String name2 = "Test" + pattern + UUID.randomUUID();
		bottle2.setName(name2);
		bottle2.setType(Bottle.Type.SIPPER);
		bottles.add(bottle2);
		
		String name = pattern + UUID.randomUUID();
		Bottle bottle1 = new Bottle();
		bottle1.setName(name);
		bottle1.setType(Bottle.Type.SIPPER);
		bottles.add(bottle1);
		
		return bottles;
	}
	
	public static User getUser(String loginName, String firstName, String middleName) {
		User user = new User();
		user.setFirstName(firstName);
		user.setMiddleName(middleName);
		user.setLastName("M");
		user.setLoginName(loginName);
		user.setDob(new Date());
		user.setBottles(UserDAOImplTest.getBottles());
		return user;
	}
}
