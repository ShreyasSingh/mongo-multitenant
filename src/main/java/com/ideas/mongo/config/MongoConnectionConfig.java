package com.ideas.mongo.config;

import java.util.Objects;

public class MongoConnectionConfig {
//    This class is basically a wrapper over MongoConfigProperties.
    private String mongoURI;
    private String defaultDatabase;

    public MongoConnectionConfig(String mongoURI) {
        this.mongoURI = "mongodb://" + mongoURI + ":27017";
        this.defaultDatabase = "mongo-multitenant";
    }

    public String getMongoURI() {
        return mongoURI;
    }

    public String getDefaultDatabase() {
        return defaultDatabase;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MongoConnectionConfig that = (MongoConnectionConfig) o;
        return mongoURI.equals(that.mongoURI) &&
                defaultDatabase.equals(that.defaultDatabase);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mongoURI, defaultDatabase);
    }
}
