package com.ideas.mongo.controller;

import com.ideas.mongo.TenantContext;
import com.ideas.mongo.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/v1/user")
public class UserController {
	
	@Autowired
	private IUserService userService; 
	
	@RequestMapping(value = "/{loginName}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<? extends Object> getUser(@PathVariable String loginName, @RequestParam String tenantCode) {
		TenantContext.setTenant(tenantCode);
		try {
			return new ResponseEntity<>(userService.getUser(loginName), HttpStatus.OK);
		} catch (Exception e) {
			BaseResponse response = new BaseResponse();
			response.setCode(ResponseCode.ERROR);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
}
