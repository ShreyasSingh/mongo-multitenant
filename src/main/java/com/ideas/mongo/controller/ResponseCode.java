package com.ideas.mongo.controller;

public interface ResponseCode {
	String SUCCESS = "Success";
	String ERROR = "Error";
}
