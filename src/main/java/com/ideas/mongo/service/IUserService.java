package com.ideas.mongo.service;

import com.ideas.mongo.dto.UserDTO;

public interface IUserService {

	void saveUser(UserDTO userDTO) throws Exception;
	UserDTO getUser(String loginName) throws Exception;
	void updateUser(UserDTO userDTO) throws Exception;
	void deleteUser(String loginName) throws Exception;
}
