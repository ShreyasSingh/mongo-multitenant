package com.ideas.mongo;

import com.ideas.mongo.config.MongoConnectionConfig;

import java.util.HashMap;
import java.util.Map;

public class TenantContext {

	private static final Map<String, MongoConnectionConfig> clusterIdMappings = new HashMap<>();
	static {
//		ideally, these configurations would be stored as application properties / or on a shared db
		clusterIdMappings.put("Choice", new MongoConnectionConfig("didnkus01ina6"));
		clusterIdMappings.put("OnPrem-NGI", new MongoConnectionConfig("mn4qg3xenvw010.ideasdev.int"));
		clusterIdMappings.put("Wyndham", new MongoConnectionConfig("localhost"));
	}

	private static final String DEFAULT_TENANT = "Wyndham";
	
	private static final ThreadLocal<String> tenant = new ThreadLocal<>();

	public static final MongoConnectionConfig getTenant() {
		String tenantCode = tenant.get();
		if(clusterIdMappings.get(tenantCode) == null) {
			tenant.set(DEFAULT_TENANT);
			tenantCode = tenant.get();
		}
		return clusterIdMappings.get(tenantCode);
	}
	
	public static final void setTenant(String tenantCode) {
		tenant.set(tenantCode);
	}

}
