package com.ideas.mongo;

import com.ideas.mongo.config.MongoDBCluster1Properties;
import com.ideas.mongo.config.MongoDBCluster2Properties;
import com.ideas.mongo.config.MongoTenantTemplate;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

@SpringBootApplication
@ComponentScan(basePackages = { "com.ideas.mongo.config", "com.ideas.mongo.controller", "com.ideas.mongo.dao", "com.ideas.mongo.service.impl" })
public class Application {

	@Autowired
	public MongoDBCluster1Properties mongoDBCluster1Properties;
	@Autowired
	public MongoDBCluster2Properties mongoDBCluster2Properties;

	@Bean
	public MongoTemplate mongoTemplate() {
// this is the default mongoTemplate that would be used in the app
		return new MongoTenantTemplate(
				new SimpleMongoDbFactory(new MongoClient(new MongoClientURI(mongoDBCluster1Properties.getUri())),
						mongoDBCluster1Properties.getDefaultDatabaseName()));
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
