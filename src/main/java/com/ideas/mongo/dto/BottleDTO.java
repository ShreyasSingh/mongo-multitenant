package com.ideas.mongo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ideas.mongo.dao.Bottle;

@JsonIgnoreProperties(ignoreUnknown=true)
public class BottleDTO {

	private Bottle.Type type;
	private String name;
	
	public Bottle.Type getType() {
		return type;
	}
	public void setType(Bottle.Type type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
